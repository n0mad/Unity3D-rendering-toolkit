<h2>Rendering toolkit for Unity 3D</h2>

Available at the official [Unity Asset Store](https://assetstore.unity.com/packages/tools/utilities/rendering-setup-toolkit-108518).

<img src="https://codeberg.org/n0mad/Unity3D-rendering-toolkit/raw/branch/master/img/rst-screenshot.png">

<br>A small and simple tool for Unity3D that has all the essential rendering parameters in one window, so you can quickly setup all the visual things you need to start working in your scene.

## Features

The tool has controls for:

- Render path.
- Color space.
- Environment lighting source: skybox, gradient, solid.
- Shadow rendering settings.
- Camera background.
- Camera HDR control.
- Light color.
- Light intensity.
- Light shadows.
- Light shadow strength.

## Install

Download the project from this repo into a temporal folder, and then move the Editor folder to your project's main folder.

```shell
git clone https://codeberg.org/n0mad/Unity3D-rendering-toolkit.git

```


## Afterword

Notice something incorrectly described, buggy or outright wrong? Then please, open an issue.
Thanks for using it (: If this was useful for you and you'd like to give back, consider donating. Donations give me the time to improve and create content. Can't donate? No worries, check out my itch.io profile, star the repo and share it!

<a href="https://ko-fi.com/n0madcoder"><img src="https://img.shields.io/badge/donate-ko--fi-orange"></a>
<a href="https://paypal.me/n0madcoder"><img src="https://img.shields.io/badge/donate-paypal-blue"></a>
<a href="https://n0madcoder.itch.io/"><img src="https://img.shields.io/badge/profile-itchio-red"></a>

